# toolbelt-meta
[![pipeline status](https://gitlab.com/python-toolbelt/toolbelt-meta/badges/main/pipeline.svg)](https://gitlab.com/python-toolbelt/toolbelt-meta/-/commits/main)
[![coverage report](https://gitlab.com/python-toolbelt/toolbelt-meta/badges/main/coverage.svg)](https://gitlab.com/python-toolbelt/toolbelt-meta/-/commits/main)

*Collection of meta-programming related utils*

**This is the readme for developers.** The documentation for users is available here: [https://python-toolbelt.gitlab.io/toolbelt-meta](https://python-toolbelt.gitlab.io/toolbelt-meta)

## Want to contribute?

Contributions are welcome! Simply fork this project on gitlab, commit your contributions, and create merge requests.

Here is a non-exhaustive list of interesting open topics: [https://gitlab.com/python-toolbelt/toolbelt-meta/-/issues](https://gitlab.com/python-toolbelt/toolbelt-meta/-/issues)

## Requirements for builds

Install requirements for setup beforehand using

```bash
poetry install -E test -E build
```

## Running the tests

This project uses `pytest`.

```bash
pytest
```

## License

This project is licensed under the terms of the MIT license. See [LICENSE](https://gitlab.com/python-toolbelt/toolbelt-meta/-/blob/main/LICENSE) for more information.
