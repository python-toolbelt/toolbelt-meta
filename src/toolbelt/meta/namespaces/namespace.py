from __future__ import annotations

from typing      import Dict, Any, Optional
from collections import MutableMapping, UserDict

class Namespace(MutableMapping):
  @property
  def module(self) -> Optional[str]:
    return self.get('__module__')

  @property
  def qualname(self) -> str:
    return self.get('__qualname__')

  @property
  def annotations(self) -> Dict[str, Any]:
    return self.get('__annotations__', {})

class SimpleNamespace(UserDict, Namespace):
  pass
