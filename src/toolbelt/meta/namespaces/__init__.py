from .namespace            import Namespace, SimpleNamespace
from .contextual_namespace import (
  ContextualNamespace, NamespaceProcessingProvider, namespace, prepare_for
)
