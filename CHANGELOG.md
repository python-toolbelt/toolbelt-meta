# Changelog

Tracking changes in `toolbelt-meta` between versions. For a complete view of all the releases, visit GitLab:

https://gitlab.com/python-toolbelt/toolbelt-meta/-/releases

## next release
- added `namespace` and especially `contextual_namespace` to unlock new ways of designing an API
- added `will_be_direct_instance_of` utility function
