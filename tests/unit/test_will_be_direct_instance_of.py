from toolbelt import meta

class TestType(type):
  def __new__(mcls, name, bases, namespace):
    super_new = super().__new__

    namespace['is_direct_instance_of_test_type'] = meta.will_be_direct_instance_of(mcls, bases)

    return super_new(mcls, name, bases, dict(namespace))

class BaseClass(metaclass = TestType): pass
class ChildClass(BaseClass):           pass

def test_will_be_direct_instance_of():
  assert BaseClass.is_direct_instance_of_test_type  is True
  assert ChildClass.is_direct_instance_of_test_type is False

