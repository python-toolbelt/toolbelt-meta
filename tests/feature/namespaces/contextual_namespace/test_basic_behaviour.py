from __future__ import annotations

from collections import defaultdict

import pytest
from toolbelt.contrib.pytest import known_bug

from toolbelt.meta.namespaces import ContextualNamespace, namespace, prepare_for

class CharacterSheetNamespace(ContextualNamespace):
  attributes = namespace()

  @namespace
  def skills(self):
    return defaultdict(int)

class CharacterSheetType(type):
  __prepare__ = prepare_for(CharacterSheetNamespace)

  def __new__(mcls, name, bases, namespace: CharacterSheetNamespace):
    return super().__new__(mcls, name, bases, { 'namespace': namespace })

class CharacterSheet(metaclass = CharacterSheetType): pass


def test_changing_context():
  class TestedCharacter(CharacterSheet):
    name = 'Jared'

    with attributes:
      dexterity: int = 6

    with skills:
      wisdom: int
      insight = 18

    with attributes:
      strength = 10

    race: str = 'Dwarf'

  actual            = TestedCharacter.namespace
  actual_skills     = actual.skills.namespace
  actual_attributes = actual.attributes.namespace

  expected_main = {
    'name':            'Jared',
    'race':            'Dwarf',
    '__annotations__': { 'race': 'str' },
    '__module__':      'test_basic_behaviour',
    '__qualname__':    'test_changing_context.<locals>.TestedCharacter'
  }
  expected_skills = {
    'insight':         18,
    '__annotations__': { 'wisdom': 'int' }
  }
  expected_attributes = {
    'strength':        10,
    'dexterity':       6,
    '__annotations__': { 'dexterity': 'int' }
  }

  assert dict(actual)                == expected_main
  assert actual.skills.namespace     == expected_skills
  assert actual.attributes.namespace == expected_attributes

  assert isinstance(actual_skills,     defaultdict)
  assert isinstance(actual_attributes, dict)

def test_shadowing_gate():
  with pytest.raises(AttributeError, match = '__enter__'):
    class TestedCharacter(CharacterSheet):
      attributes = None

      with attributes:
        strength = 10

def test_alternative_gate_access():
  class TestedCharacter(CharacterSheet):
    attributes = None

    with _gates_['attributes']:
      strength = 10

  actual            = TestedCharacter.namespace
  actual_attributes = actual.attributes.namespace

  expected_main = {
    'attributes':   None,
    '__module__':   'test_basic_behaviour',
    '__qualname__': 'test_alternative_gate_access.<locals>.TestedCharacter'
  }
  expected_attributes = { 'strength': 10 }

  assert dict(actual)      == expected_main
  assert actual_attributes == expected_attributes

@known_bug(issue_url = 'https://gitlab.com/micuda-python/toolbelt-meta/-/issues/1')
def test_namespace_without_namespace_gate_does_not_raise():
  class TestedNamespace(ContextualNamespace): pass

  class FooType(type):
    __prepare__ = prepare_for(TestedNamespace)

    def __new__(mcls, name, bases, namespace: TestedNamespace):
      return super().__new__(mcls, name, bases, { 'namespace': namespace })

  class Foo(metaclass = FooType): pass
